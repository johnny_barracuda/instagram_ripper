# Requirements
- This project requires python 3.6 or greater
- requires [ripme](https://github.com/RipMeApp/ripme)

## How to use
Download the [zip file here](https://gitlab.com/johnny_barracuda/instagram_ripper/-/archive/master/instagram_ripper-master.zip). Unzip the project and navigate to `src/`. In there you will find the `instagram_ripper.py` file. Move that file to wherever you desire it to be. For the script to work it requires some information. It has been commented to explain what information is needed and how to add it. Open it with a text editor to read the comments and fill in the required info. The main 2 requirements are the locations where the folder containing all the instagram user directories are located, and where the ripme.jar is located. Fill in those file paths (preferably using absolute file paths). Once that is setup all you need to do to run it is (while using python 3.6) from your CLI enter in `python3.6 instagram_ripper.py`