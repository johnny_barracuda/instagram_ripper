#!/user/bin/env python3.6

import os
import re
import subprocess
import shutil
import time

from datetime import datetime as dt
from multiprocessing import Process, Queue, current_process, freeze_support

# instagram_ripper.py is given a path with directories of names of instagram
# users, then uses those names to download and update their photos and videos.
#
# To run this script, if you are on Linux make sure this file has executable permissions
# by running the command 'sudo chmod +x instagram_ripper.py'. Then run './instagram_ripper.py'
# in the directory where 'instagram_ripper.py' is located
#
# If you are on Windows or want to run the script this way on Linux, in your CLI
# type 'python3.6 instagram_ripper.py'. This script requires python version 3.6
# or greater
#
# For this script to work you will need to enter in the location of certain directories
# the easiest way to do this is open up the directory in your CLI and print out the location.
# For example on Windows, while in the location in your command prompt type 'cd' to
# print out the folder path
#
# on Linux open up the directory in your terminal and type 'pwd' to get the directory
# path
#
# Do this for each variable below that has the name 'LOCATION' in it.

# INSTAGRAM_DIRECTORY_LOCATION is the location where the instagram user directories
# are located. The directories contained here should be the names of the instagram
# users name. For example David Beckhams instagram is
# 'https://www.instagram.com/davidbeckham/'.
# his directory in this location would simply be a folder called 'davidbeckham'
INSTAGRAM_DIRECTORY_LOCATION = ''

# This is the location on your computer that contains the ripme.jar file. This script
# also assumes that the default 'rips' directory that holds the downloaded files is
# located in this same directory. If you have saved a custom location you should change
# it to this location
RIPME_LOCATION = ''

# This script uses threading to greatly reduce the total download time. The more threads
# the quicker the overall download will go. However there are marginal returns and the more
# threads used will greatly increase CPU and RAM usage. A good rule of thumb is to set
# this number to how many cores your CPU has (for example the average CPU has 4 cores)
#
# play around with this number by increasing and decreasing it until you find what works
# best for you
NUMBER_OF_THREADS = 2

# if REMOVE_ALREADY_SAVED is set to 'True', then only the photos that come after the most
# recent photo that is saved on the computer will be added to the directory. Everything that
# came before the most recent photo will be deleted. This is helpful if you have cleaned up
# a folder, and dont want older deleted picture to accidently be re-added
REMOVE_ALREADY_SAVED = True

_regex_insta_pattern = re.compile(r'^(\d{4})_(\d{2})_(\d{2})_(\d{2})_(\d{2})_(.*)')

def instagram_directory_renamer():
    '''It a directory contains the 'instagram_' prefix it removes the prefix'''
    os.chdir(os.path.abspath(INSTAGRAM_DIRECTORY_LOCATION))

    for dir in os.listdir():
        if bool(re.search('^instagram_', dir)):
            os.rename(dir, re.sub('^instagram_', '', dir))

def get_usernames():
    '''
    Navigates to the INSTAGRAM_DIRECTORY_LOCATION and returns a list
    of all usernames stored in that location
    '''
    usernames = []
    os.chdir(INSTAGRAM_DIRECTORY_LOCATION)
    for user in os.listdir():
        usernames.append(user)
    return usernames

def most_recent_saved_insta_pic(user):
    '''
    Naviagates to the directory that contains already downloaded pics/vids.
    It finds all files by using a regular expression to find the instagram file
    format. It then places all found files in a list, sorts it and returns the last
    file within the list (the most recent one by date)
    '''

    all_files = []
    for root, dirs, files in os.walk(os.path.abspath(f'{INSTAGRAM_DIRECTORY_LOCATION}/{user}')):
        for file in files:
            if os.path.isfile(os.path.join(root, file)):
                if bool(_regex_insta_pattern.search(file)):
                    all_files.append(file)

    all_files.sort()
    if len(all_files) >= 1:
        return all_files[-1]
    else:
        # if no file is found that has the instagram file format, a string of
        # exit is returned to indicate none were found
        return 'exit'

def download_instagram_user(user_queue, done_queue):
    '''
    Downloads the instagram user and if REMOVE_ALREADY_SAVED is set to True deletes
    all files that are older than what the most recently saved file is (only the new
    files will remain)
    '''
    for user in iter(user_queue.get, 'STOP'):
        if most_recent_saved_insta_pic(user) != 'exit':
            os.chdir(os.path.abspath(RIPME_LOCATION))
            insta_user_url = f'https://www.instagram.com/{user}'
            subprocess.run(['java', '-jar', 'ripme.jar', '-u', insta_user_url])

            # change directories to where the newly downloaded files are
            if os.path.exists(os.path.abspath(f'{RIPME_LOCATION}/rips/instagram_{user}')):
                os.chdir(os.path.abspath(f'{RIPME_LOCATION}/rips/instagram_{user}'))

            if REMOVE_ALREADY_SAVED == True:
                for file in os.listdir():

                    # these variable will be used to make sure any pictures older then
                    # the oldest already downloaded will be deleted to avoid duplication and
                    # prevent removed pictures from being re-downloaded
                    most_recent_saved_pic_year = _regex_insta_pattern.match(most_recent_saved_insta_pic(user)).group(1)
                    most_recent_saved_pic_month = _regex_insta_pattern.match(most_recent_saved_insta_pic(user)).group(2)
                    most_recent_saved_pic_day = _regex_insta_pattern.match(most_recent_saved_insta_pic(user)).group(3)
                    most_recent_saved_pic_hour = _regex_insta_pattern.match(most_recent_saved_insta_pic(user)).group(4)
                    most_recent_saved_pic_minute = _regex_insta_pattern.match(most_recent_saved_insta_pic(user)).group(5)

                    # Assemble the differnt variables into a string
                    most_recent_tmp = f'{most_recent_saved_pic_year}/{most_recent_saved_pic_month}/{most_recent_saved_pic_day}/{most_recent_saved_pic_hour}/{most_recent_saved_pic_minute}'
                    # Turn the string into a date time object
                    most_recent_saved_date = dt.strptime(most_recent_tmp, '%Y/%m/%d/%H/%M')

                    # Extract each part of the filename to individule variables. these
                    # variables will be used to create a python datetime string for easy
                    # comparisons with the most recently saved file
                    file_year = _regex_insta_pattern.match(file).group(1)
                    file_month = _regex_insta_pattern.match(file).group(2)
                    file_day = _regex_insta_pattern.match(file).group(3)
                    file_hour = _regex_insta_pattern.match(file).group(4)
                    file_minute = _regex_insta_pattern.match(file).group(5)

                    # Assemble the differnt variables into a string
                    file_tmp = f'{file_year}/{file_month}/{file_day}/{file_hour}/{file_minute}'
                    # Turn the string into a date time object
                    file_date = dt.strptime(file_tmp, '%Y/%m/%d/%H/%M')

                    # if the current file that is being iterated over is older than
                    # the most recent file that is saved, it is removed. This
                    # means that the only files that will remain will be more recent (newer)
                    # then what is currently saved.
                    if file_date < most_recent_saved_date:
                        os.remove(file)

            for file in os.listdir():
                # moves all recent files that have been downloaded to their destination
                # directory
                destination = os.path.abspath(f'{INSTAGRAM_DIRECTORY_LOCATION}/{user}')
                shutil.move(os.path.join(os.getcwd(), file),
                            os.path.join(destination, file))

            # Removes the now empty folder for cleanup
            if os.path.exists(os.path.abspath(f'{RIPME_LOCATION}/rips/instagram_{user}')):
                os.removedirs(os.path.abspath(f'{RIPME_LOCATION}/rips/instagram_{user}'))

def main():
    start_time = time.time()
    print('Renaming Instagram directories (if needed)')
    instagram_directory_renamer()

    print("Making Queues")
    username_queue = Queue()
    done_queue = Queue()

    print("Filling Queue")
    usernames = get_usernames()
    for user in usernames:
        username_queue.put(user)

    print("Starting Downloads")
    for i in range(NUMBER_OF_THREADS):
        Process(target=download_instagram_user,
                args=(username_queue, done_queue)).start()

    for i in range(NUMBER_OF_THREADS):
        username_queue.put('STOP')

    while not username_queue.empty():
        time.sleep(2)

    print("Update Complete!")
    end_time = time.time()
    print(f'It took {end_time - start_time:.2f} seconds to complete')


if __name__ == '__main__':
    main()
